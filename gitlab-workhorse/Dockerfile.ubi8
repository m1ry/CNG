ARG GITLAB_BASE_IMAGE=

FROM ${GITLAB_BASE_IMAGE} as build

ARG GITLAB_USER=git
ARG UID=1000

ADD gitlab-workhorse-ee.tar.gz /assets
ADD gitlab-gomplate.tar.gz /assets
ADD gitlab-exiftool.tar.gz /assets

COPY scripts/ /assets/scripts/

RUN install -d -o ${UID} -g 0 -m 0770 /assets/var/log/gitlab \
    install -d -o ${UID} -g 0 -m 0770 /assets/srv/gitlab/config \
    && chown -R ${UID}:0 /assets/srv/gitlab \
    && chmod -R g=u /assets

## FINAL IMAGE ##

FROM ${GITLAB_BASE_IMAGE} as final

ARG GITLAB_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG GITLAB_DATA=/var/opt/gitlab
ARG DNF_OPTS

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-workhorse" \
      name="GitLab Workhorse" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Gitlab Workhorse is a smart reverse proxy for GitLab." \
      description="Gitlab Workhorse is a smart reverse proxy for GitLab. It handles large HTTP requests."

ENV LANG=C.UTF-8

COPY --from=build /assets/ /

RUN microdnf ${DNF_OPTS} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 perl shadow-utils \
    && microdnf clean all \
    && install -d -o ${UID} -g 0 -m 0770 /home/${GITLAB_USER} \
    && install -d -o ${UID} -g 0 -m 0770 ${GITLAB_DATA} \
    && adduser -M -d /home/${GITLAB_USER} -u ${UID} ${GITLAB_USER} \
    # remove shadow-utils within the same layer
    && microdnf remove shadow-utils \
    && microdnf clean all

USER ${UID}

CMD ["/scripts/start-workhorse"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
