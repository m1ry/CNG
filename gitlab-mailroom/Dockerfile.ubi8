## FINAL IMAGE ##

ARG RUBY_IMAGE=

FROM ${RUBY_IMAGE}

ARG MAILROOM_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG DNF_OPTS

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-mailroom" \
      name="GitLab Mailroom" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${MAILROOM_VERSION} \
      release=${MAILROOM_VERSION} \
      summary="A configuration based process that will idle on IMAP connections and execute a delivery method when a new message is received." \
      description="A configuration based process that will idle on IMAP connections and execute a delivery method when a new message is received."

RUN microdnf ${DNF_OPTS} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 procps libicu tzdata shadow-utils \
    && microdnf clean all \
    && adduser -m ${GITLAB_USER} -u ${UID} \
    # remove shadow-utils within the same layer
    && microdnf remove shadow-utils \
    && microdnf clean all

COPY scripts/ /scripts/

RUN chgrp -R 0 /scripts /home/${GITLAB_USER} && \
    chmod -R g=u /scripts /home/${GITLAB_USER}

ADD gitlab-mailroom.tar.gz /

USER ${UID}

CMD ["/usr/bin/mail_room", "-c", "/var/opt/gitlab/mail_room.yml", "--log-exit-as", "json"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
